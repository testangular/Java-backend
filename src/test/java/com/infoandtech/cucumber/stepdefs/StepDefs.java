package com.infoandtech.cucumber.stepdefs;

import com.infoandtech.InfoandtechApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = InfoandtechApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
